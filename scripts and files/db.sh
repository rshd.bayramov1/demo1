#!/usr/bin/env bash

sudo apt-get update --fix-missing -y && sudo apt-get install -qq mysql-server

green() {
  echo -e '\e[32m'$1'\e[m';
}

DBNAME=petclinic
DBUSER=DevOps
DBPASSWD=AzDevOps2@2@
ROOTPASSWD=AzDevOps2@2@

MYSQL=`which mysql`

# Construct the MySQL query
Q1="CREATE DATABASE IF NOT EXISTS $DBNAME;"
Q2="CREATE USER IF NOT EXISTS '$DBUSER'@'172.16.10.%' IDENTIFIED BY '$DBPASSWD';"
Q3="GRANT ALL ON $DBNAME.* TO '$DBUSER'@'172.16.10.%';"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

# Run the actual command
sudo $MYSQL -uroot -p$ROOTPASSWD -e "$SQL"

# Let the user know the database was created
green "Database $DBNAME and user $DBUSER created with a password you chose"

#Set bind address because of not only connection from localhost
sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

sudo service mysql restart
