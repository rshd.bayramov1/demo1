#!/usr/bin/env bash

sudo apt-get update --fix-missing -y && sudo apt-get install openjdk-14-jdk -y

function clone_pull {
    Dir=$(basename "$1" .git)
    if [[ -d "$Dir" ]]; then
      cd $Dir
      git pull
    else
      git clone "$1" && cd $Dir
    fi
}

#Clone my Gitlab project
clone_pull https://gitlab.com/rshd.bayramov1/demo1.git

sudo chmod +x mvnw

./mvnw clean package

#Copy spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar new APP_USER  home folder 
cp /home/rashad/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar /home/rashad/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar
